#include "Interface.h"

int get_num_of_processes(FILE **fp){
    int num_of_processes = 1;
    int c;
    for (c = getc(*fp); c != EOF; c = getc(*fp))
        if (c == '\n') num_of_processes = num_of_processes + 1;
    return num_of_processes;
}

void load_process_in_queue(int num_of_processes, pcb_t *queue_of_processes, FILE **fp){
    for(int i = 0; i<num_of_processes;i++){
        fscanf(*fp, "%s %d %d", queue_of_processes[i].process_name,&queue_of_processes[i].entryTime, &queue_of_processes[i].serviceTime);
        queue_of_processes[i].state = READY;
        queue_of_processes[i].remainingTime = queue_of_processes[i].serviceTime;
        queue_of_processes[i].checked_in = 0;
        queue_of_processes[i].lastEntry = queue_of_processes[i].entryTime;
        queue_of_processes[i].totalwait = 0;
    }
}


void sort(int size_of_queue, pcb_t *processes_queue){
    int i,j;
    pcb_t temp;
    for (i = 0; i < size_of_queue; ++i){
        for (j = i + 1; j < size_of_queue; ++j){
            if (processes_queue[i].entryTime > processes_queue[j].entryTime){
                temp =  processes_queue[i];
                processes_queue[i] = processes_queue[j];
                processes_queue[j] = temp;

            }
        }
    }
}


pcb_t * get_queue_of_processes(int *num_of_processes,char *filename){
    FILE *fp;
    pcb_t *queue_of_processes;
    /* Open the file to find number of processes needed to be processed*/
    fp = fopen(filename,"r");
    /* Find the number of processes needed */
    *num_of_processes = get_num_of_processes(&fp);
    fclose(fp);
    /* Open the file to load processes in queue to serve them based on their entrytime */
    fp = fopen(filename,"r");
    /* Initialize buffer to keep processes */
    queue_of_processes = (pcb_t*) malloc(*num_of_processes*sizeof(pcb_t));
    /* Load queue with the processes */
    load_process_in_queue(*num_of_processes,queue_of_processes,&fp);
    sort(*num_of_processes,queue_of_processes);
    fclose(fp);
    return queue_of_processes;
}

Queue* initQueue(unsigned max_queue_size){
    /* Initializing pointer to the circular queue struct */
    Queue* queue = (Queue*) calloc(1,sizeof(Queue));
    /* Initialize an empty queue */
    queue->queue_size = max_queue_size;
    queue->front = 0;
    queue->len = 0;
    queue->rear = max_queue_size - 1;
    /* Create the array which will act as the queue*/
    queue->queue_array = (pcb_t*) calloc(queue->queue_size, sizeof(pcb_t));
    return queue;
}

int isEmpty(Queue* queue){
    return (queue->len == 0);
}


void push(Queue *queue, pcb_t item){
    /* Adds items at the back of the queue and increase len of queue by 1*/
    queue->rear = (queue->rear + 1) % queue->queue_size;
    queue->queue_array[queue->rear] = item;
    queue->len = queue->len + 1;
}

pcb_t serve(Queue *queue){
    /* Return the first item from the queue*/
    pcb_t item = queue->queue_array[queue->front];
    queue->front = (queue->front + 1)%queue->queue_size;
    queue->len = queue->len - 1;
    return item;
}
void waitForProcesses(Queue *queue, pcb_t * available_processes,int * pTo_avail_proc,int time, int num_of_processes){
    pcb_t next_process = available_processes[*pTo_avail_proc];
    while(next_process.entryTime <= time && *pTo_avail_proc != num_of_processes){

        /* Add name of next process in y-axis */
        next_process.row_num = appendRow(next_process.process_name);
        /* Add empty bar to show when process did not appear */
        appendBlank(next_process.row_num,next_process.entryTime);

        printf("%s has entered at %d time</n>",next_process.process_name,next_process.entryTime);
        push(queue, next_process);
        *pTo_avail_proc += 1;
        next_process = available_processes[*pTo_avail_proc];
    }
}

