#include "Interface.h"

void RoundRobin(char * filename){
    int num_of_processes, pTo_avail_proc, time, first_process;
    int *pNum_Of_Processes = &num_of_processes;
    pcb_t * available_processes;
    Queue* queue = initQueue(10);
    available_processes = get_queue_of_processes(pNum_Of_Processes,filename);

    pTo_avail_proc = 0;
    /* Put the first process in the queue */
    push(queue, available_processes[0]);
    pTo_avail_proc += 1;
    time = 0;
    first_process = TRUE;
     /* Init the window */
    initInterface("white", "grey");
    while(!isEmpty(queue)){
        /* Get the latest process ready */
        pcb_t current_process = serve(queue);
        current_process.state = RUNNING;

        if(first_process){
            time = current_process.entryTime;
            
            /* Add name of current process in y-axis */
            current_process.row_num = appendRow(current_process.process_name);
            /* Add a blank bar until the process starts */
            appendBlank(current_process.row_num,current_process.entryTime);
            
            first_process = FALSE;
        }
        /* Serve the process */
        if(current_process.remainingTime <= QUANTUM){
            printf("%s is in running state</n>",current_process.process_name);
            current_process.totalwait += (time-current_process.lastEntry);
            appendBar(current_process.row_num,time-current_process.lastEntry,"black",current_process.process_name,1);
            appendBar(current_process.row_num,current_process.remainingTime,"black",current_process.process_name,0);
            
            time += current_process.remainingTime;
            
            sleep(current_process.remainingTime);
            current_process.state = EXIT;
            int turnaround = time - current_process.entryTime;
            printf("%s completed. Turnaround time: %d seconds. Total wait time: %d seconds </n>",current_process.process_name,turnaround,current_process.totalwait);
        }else{
            printf("%s is in running state.</n>",current_process.process_name);
            current_process.totalwait += (time-current_process.lastEntry);
            appendBar(current_process.row_num,time-current_process.lastEntry,"black",current_process.process_name,1);
            appendBar(current_process.row_num,QUANTUM,"black",current_process.process_name,0);
            
            current_process.remainingTime -= QUANTUM;
            sleep(QUANTUM);
            current_process.state = READY;
            time += QUANTUM;
            
            current_process.lastEntry = time;
        }
        /* Adds all the processes that have entered inside the ready queue during the service of the current process */
        if(pTo_avail_proc != num_of_processes){
            waitForProcesses(queue,available_processes,&pTo_avail_proc,time,num_of_processes);
        }
        /* Add the process which still has not exited to the back of the queue*/
        if(current_process.state == READY){
            push(queue, current_process);
        }
        if(isEmpty(queue) && pTo_avail_proc < num_of_processes){
            while(time < available_processes[pTo_avail_proc].entryTime){
                time += 1;
            }
            waitForProcesses(queue,available_processes,&pTo_avail_proc,time,num_of_processes);
        }

    }
    waitExit();
    free(available_processes);
}
