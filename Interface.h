#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <limits.h>
#include "process-visualiser.h"

#define TRUE 1
#define FALSE 0
#define QUANTUM 2

typedef enum{
    READY,RUNNING,EXIT
} process_state_t;

typedef struct{
    char process_name[11];

    int entryTime;
    int serviceTime;
    int remainingTime;
    
    int lastEntry;

    int totalwait;
    
    int checked_in;
    int row_num;
    process_state_t state;
} pcb_t;

typedef struct
{
    int front, rear, len;
    unsigned queue_size;
    pcb_t* queue_array;
} Queue;

/* Gets the number of processes in the file */
int get_num_of_processes(FILE **fp);
/* Loads all the processes in the files into the memory queue*/
void load_process_in_queue(int num_of_processes, pcb_t *queue_of_processes, FILE **fp);
/* Sorts using insertion sort based on the entry time of the processes*/
void sort(int size_of_queue, pcb_t *processes_queue);
/* Returns an array containing processes which need to be run*/
pcb_t * get_queue_of_processes(int *num_of_processes,char *filename);
/* Initializes the queue which will contain the processes*/
Queue* initQueue(unsigned max_queue_size);
/* Checks if the queue is empty*/
int isEmpty(Queue* queue);
/* Pushes items inside the queue structure */
void push(Queue *queue, pcb_t item);
/* Pops or serves the first item in the queue*/
pcb_t serve(Queue *queue);
/* Function that checks if there is any processes waiting while a process is running */
void waitForProcesses(Queue *queue, pcb_t * available_processes,int * pTo_avail_proc,int time, int num_of_processes);
/* Round Robin algorithm to schedule processes */
void RoundRobin(char * filename);
/* FIFO algorithm for scheduling processes */
void FIFO(char *filename);



