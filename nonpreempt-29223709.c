#include "Interface.h"

void FIFO(char *filename){
    int num_of_processes, pTo_avail_proc, time, first_process;
    int *pNum_Of_Processes = &num_of_processes;
    pcb_t * available_processes;
    Queue* queue = initQueue(10);
    available_processes = get_queue_of_processes(pNum_Of_Processes,filename);

    pTo_avail_proc = 0;
    /* Put the first process in the queue */
    push(queue, available_processes[0]);
    pTo_avail_proc += 1;
    time = 0;
    first_process = TRUE;

    /* Init the window */
    initInterface("white", "grey");
    
    while(!isEmpty(queue)){
        /* Get the latest process ready */
        pcb_t current_process = serve(queue);
        current_process.state = RUNNING;

        if(first_process){
            /* Add name of current process in y-axis */
            current_process.row_num = appendRow(current_process.process_name);
            /* Add a blank bar until the process starts */
            appendBlank(current_process.row_num,current_process.entryTime);

            time = current_process.entryTime;
            first_process = FALSE;
        }
        printf("%s is in running state</n>",current_process.process_name);
        sleep(current_process.serviceTime);

        /* Add the bar to represent how long process has been serviced */
        int waittime = time-current_process.entryTime;
        appendBar(current_process.row_num,waittime,"black",current_process.process_name,1);
        appendBar(current_process.row_num,current_process.serviceTime,"black",current_process.process_name,0);
        
        time += current_process.serviceTime;
        int turnaround = time - current_process.entryTime;
        printf("%s completed.Turnaround time: %d seconds. Total wait time: %d seconds.</n>",current_process.process_name,turnaround,waittime);
        current_process.state = EXIT;

        /* Adds all the processes that have entered inside the ready queue during the service of the current process */
        if(pTo_avail_proc != num_of_processes){
            waitForProcesses(queue,available_processes,&pTo_avail_proc,time,num_of_processes);

        }
        //printf("%s has completed at %d time\n",current_process.process_name,time);
        if(isEmpty(queue) && pTo_avail_proc < num_of_processes){
            while(time < available_processes[pTo_avail_proc].entryTime){
                time += 1;
            }
            waitForProcesses(queue,available_processes,&pTo_avail_proc,time,num_of_processes);
        }

    }
    waitExit();
    free(available_processes);

}

